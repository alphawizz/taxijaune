//
//  TJUserModel.swift
//  Taxi Jaune
//
//  Created by shaifali bangar on 23/05/20.
//  Copyright © 2020 shaifali bangar. All rights reserved.
//

import Foundation


class TJUser {
    
    var loginInfoObj: loginData!
    static let shared = TJUser()
    
    func getUserInfo() {
        
        if let decoded  = UserDefaults.standard.object(forKey: TJUserKey.LoginData) as? Data {
            
            do {
                let dictUserDetail = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as! [String: Any]
                self.loginInfoObj = loginData.init(json: dictUserDetail)
                
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}

class loginData: Codable {
    
    var first_name: String?
    var last_name: String?
    var email: String?
    var bio: String?
    var dob: String?
    var phone_number: String?
    var userID: String?
    var image: String?
    
    init(json: [String: Any]) {
        
//        first_name = json.getString(forKey: TJJsonKey.first_name)
//        phone_number = json.getString(forKey: TJJsonKey.phone_number)
//        userID = json.getString(forKey: TJJsonKey.Id)
//        image = json.getString(forKey: TJJsonKey.image)
//        last_name = json.getString(forKey: FSJsonKey.last_name)
//        email = json.getString(forKey: TJJsonKey.email)
//        bio = json.getString(forKey: TJJsonKey.bio)
//        dob = json.getString(forKey: TJJsonKey.dob)
//        image =  json.getString(forKey: TJJsonKey.image)
      //  image = FSProfileBaseURL + imageStr
    }
}
