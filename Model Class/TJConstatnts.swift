//
//  TJConstatnts.swift
//  Taxi Jaune
//
//  Created by shaifali bangar on 23/05/20.
//  Copyright © 2020 shaifali bangar. All rights reserved.
//

import Foundation
import UIKit

//Mark:- Constant Keys for value
struct TJUserKey {
    static let LoginData = "LoginData"
}

//Mark:- Save LoggedIn User info For Login Session -
struct UserProfileData {
    static let key = "userProfile"
    static func save(_ value: loginData!) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(value), forKey: key)
    }
    
    static func get() -> loginData! {
        var userData: loginData!
        if let data = UserDefaults.standard.value(forKey: key) as? Data {
            userData = try? PropertyListDecoder().decode(loginData.self, from: data)
            return userData!
        } else {
            return userData
        }
    }
    static func remove() {
        UserDefaults.standard.removeObject(forKey: key)
    }
}

//Mark:- Base Url


//let FSBaseURL = "https://alphawizz.com/Bible/Api/Authentication/"
//let FSProfileBaseURL = "https://alphawizz.com/Bible/"
let FSDefault  = UserDefaults()

let FSBaseURL = "http://masterfiveappsstudios.com.br/bible/"
let FSProfileBaseURL = "http://masterfiveappsstudios.com.br/bible/"


//Mark:- AppSesvices
struct TJApi  {
    
    static let downloadData = "MasterTable"
    static let login = "login"
    static let registration = "registration"
    static let generateOtp = "generateOtp"
    static let otpVarification = "otpVarification"
    static let socialLogin = "SocialLogin"
    static let editProfile = "EditProfile"
     static let addHighlights = "addHighlights"
    static let deleteHighlight = "deleteHighlights"
     static let addBookMark = "addBookMark"
     static let deleteBookMark = "deleteBookMark"
  
      }

struct FSStoryBoard {
    
    static let PopUp = UIStoryboard.init(name: "FSPopUp", bundle: nil)
    static let Main = UIStoryboard.init(name: "Main", bundle: nil)
}


enum APIStatus: String {
    
    case successOK = "200"
    case success = "1"
    case NoContent = "204"
    case BadRequest = "400"
    case ServerError = "500"
    case NotFound = "404"
}



//Mark:- Json filter keys
struct TJJsonKey {
    
    //Mark --> User Table Data............
    static let Id = "id"
    static let first_name = "first_name"
    static let last_name = "last_name"
    static let dob = "dob"
    static let phone_number = "phone_number"
    static let bio = "bio"
    static let Firebase_token = "Firebase_token"
    static let email = "email"
    static let profilePic = "profilePic"
    static let gender = "gender"
      static let image = "image"
    
    //Mark --> Setting Table Data..........
    static let user_id = "user_id"
    static let create_at = "create_at"
    static let isNew = "isNew"
    static let isUpdate = "isUpdate"
    static let otp = "otp"
    static let phone = "phone"
    
    static let description = "description"
    static let title = "title"
    static let desc = "desc"
    static let highlightText = "highlightText"
    static let highlightColorCode = "highlightColorCode"
    static let data = "data"
    static let mId = "mId"
    static let isDelete = "isDelete"
    static let bookmarkText = "bookmarkText"
    static let postId = "postId"
   
}

struct TJColors {
    
    static let navigationBarColor = UIColor.init(red: 3/255, green: 139/255, blue: 207/255, alpha: 1.0)
    static let buttonSelectionColor = UIColor.init(red: 3/255, green: 139/255, blue: 207/255, alpha: 1.0)
    static let buttonDeSelectionColor = UIColor.init(red: 52/255, green: 83/255, blue: 115/255, alpha: 1.0)
    static let StrokeColor = UIColor.black
    static let RadiusFillColor = UIColor.init(red: 3/255, green: 139/255, blue: 207/255, alpha: 0.5)
    
}

struct FTJDevice {
     static let Id = UIDevice.current.identifierForVendor!.uuidString
}
           
