import UIKit
import SDWebImage

class LeftViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
        
    let kHeaderSectionTag: Int = 6900
    
    @IBOutlet var tableviewList: UITableView!
    @IBOutlet var imageviewProfile: UIImageView!
  
    @IBOutlet var labelUserName: UILabel!
    
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var window: UIWindow?
    
    //For Driver
    
//    let sectionNames = [["title":"requête récus",
//                           "image" : "Icon feather-home"],
//                                 ["title":"Mes courses",
//                                 "image" : "Icon feather-home"],
//                                 ["title":"Mon profil",
//                                 "image" : "Icon feather-home"],
//                                 ["title":"Déconnexion",
//                                 "image" : "Icon feather-home"]]
    
    
    //For Rider
    
    let sectionNames = [["title":"Accueil",
                         "image" : "Icon feather-home"],
                        
                         // My request
                        ["title":"Mes requêtes",
                        "image" : "Icon feather-send"],
                        
                         // My Booking
                        ["title":"Réservation",
                        "image" : "Group 2487"],
                        
                        // rent a vehicle
                        ["title":"Louer un véhicule",
                        "image" : "Group 2487"],
                        
                        ["title":"Véhicule loué",
                        "image" : "Group 2487"],
                        
                         // My Profile
                        ["title":"Mon profil",
                        "image" : "Icon material-person-outline"],
                        
                        ["title":"Déconnexion",
                        "image" : "Icon ionic-ios-log-out"]
                        
                      ]
                        
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableviewList.register(UINib(nibName: "SSSectionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "SSSectionHeaderView")
        
        let headerNib = UINib.init(nibName: "LeftSideMenuBar", bundle: Bundle.main)
        tableviewList.register(headerNib, forHeaderFooterViewReuseIdentifier: "LeftSideMenuBar")
        tableviewList.tableFooterView = UIView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshUserProfileData), name: Notification.Name("refreshUserProfileData"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
       
//        labelUserName.text = TJUser.shared.loginInfoObj.userName
//        imageviewProfile.layer.cornerRadius = 60
//        imageviewProfile.layer.borderColor = UIColor.white.cgColor
//        imageviewProfile.layer.borderWidth = 1
//
//
//        imageviewProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
//        imageviewProfile.sd_setImage(with: URL.init(string: TJUser.shared.loginInfoObj.userImage!)) { (image, error, cache, urls) in
//            if (error != nil) {
//                // Failed to load image
//                self.imageviewProfile.image = UIImage(named: "user")
//            } else {
//                // Successful in loading image
//                self.imageviewProfile.image = image
//            }
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false

    }
    
    @objc func refreshUserProfileData(notification: Notification) {
        
      
//        labelUserName.text = TJUser.shared.loginInfoObj.userName
//        imageviewProfile.layer.cornerRadius = 60
//        imageviewProfile.layer.borderColor = UIColor.white.cgColor
//        imageviewProfile.layer.borderWidth = 1
//
//        imageviewProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
//        imageviewProfile.sd_setImage(with: URL.init(string: TJUser.shared.loginInfoObj.userImage!)) { (image, error, cache, urls) in
//            if (error != nil) {
//                // Failed to load image
//                self.imageviewProfile.image = UIImage(named: "user")
//            } else {
//                // Successful in loading image
//                self.imageviewProfile.image = image
//            }
//        }
    }

    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .fade
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sectionNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let CellIdentifier = "Cell"
        var cell = (tableView.dequeueReusableCell(withIdentifier: CellIdentifier)) as? LeftProfileTopCell
        if cell == nil {
            let nib = Bundle.main.loadNibNamed("LeftCell", owner: self, options: nil)
            cell = nib?[0] as? LeftProfileTopCell
        }
        
        let section = self.sectionNames[indexPath.row]
        cell?.lblTitle?.text = section["title"]
        cell?.imageviewIcon?.image = UIImage.init(named: section["image"]!)

        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                return 60
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let main = UIStoryboard(name: "Main", bundle: nil)
        
        let indexPath = IndexPath.init(row: indexPath.row, section: indexPath.section)
        let cell = tableView.cellForRow(at: indexPath)
        cell?.backgroundView?.backgroundColor = UIColor.green
        
        switch indexPath.row {
            
        case 0:
            
            sideMenuController?.hideLeftView(animated: true, completionHandler: {
                  NotificationCenter.default.post(name: Notification.Name("refreshDashBoardData"), object: nil, userInfo: nil)
           })
            
        case 1:
            let productListVC = main.instantiateViewController(withIdentifier: String.init(describing: TJMyRequestViewController.self)) as! TJMyRequestViewController
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(productListVC, animated: true)

        case 2:
            let productListVC = main.instantiateViewController(withIdentifier: String.init(describing: TJMyBookingViewController.self)) as! TJMyBookingViewController
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(productListVC, animated: true)

        case 3:
            let productListVC = main.instantiateViewController(withIdentifier: String.init(describing: TJRentAVehicleViewController.self)) as! TJRentAVehicleViewController
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(productListVC, animated: true)
            
            case 4:
            let productListVC = main.instantiateViewController(withIdentifier: String.init(describing: TJRentalVehiclesListViewController.self)) as! TJRentalVehiclesListViewController
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(productListVC, animated: true)

            case 5:
            let MyProfileVC = main.instantiateViewController(withIdentifier: String.init(describing: TJMyProfileViewController.self)) as! TJMyProfileViewController
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(MyProfileVC, animated: true)
//
//            case 5:
//                   let productListVC = main.instantiateViewController(withIdentifier: String.init(describing: ALSettingViewController.self)) as! ALSettingViewController
//                   self.navigationController?.isNavigationBarHidden = true
//                   self.navigationController?.pushViewController(productListVC, animated: true)
//
//             case 6:
//            let main = UIStoryboard(name: "Main", bundle: nil)
//
//            let objHome = main.instantiateViewController(withIdentifier: "ALLoginViewController") as! ALLoginViewController
//
//            let mainNavigationVC = UINavigationController.init(rootViewController: objHome)
//             mainNavigationVC.isNavigationBarHidden = true
//            UserDefaults.standard.removeObject(forKey: ALUserKey.LoginData)
//            UIApplication.shared.keyWindow?.rootViewController = mainNavigationVC
            
        default: break
        }
    }
    
    
  
    
//    func onYesOfLogoutPopup() {
//
//        UserDefaults.standard.removeObject(forKey: GAUserKey.AddToCard)
//        UserDefaults.standard.removeObject(forKey: GAUserKey.LoginData)
//        UserDefaults.standard.removeObject(forKey: GAUserKey.SavedAddress)
//        UserDefaults.standard.synchronize()
//
//        let main = UIStoryboard(name: "Main", bundle: nil)
//
//        let objHome = main.instantiateViewController(withIdentifier: "GALoginViewController") as! GALoginViewController
//        let mainNavigationVC = UINavigationController.init(rootViewController: objHome)
//        UIApplication.shared.keyWindow?.rootViewController = mainNavigationVC
//    }
    
    
    @IBAction func onHelp(_ sender: Any) {
        
         let main = UIStoryboard(name: "Main", bundle: nil)
        
        let MyProfileVC = main.instantiateViewController(withIdentifier: String.init(describing: TJHelpViewController.self)) as! TJHelpViewController
                  self.navigationController?.isNavigationBarHidden = true
                  self.navigationController?.pushViewController(MyProfileVC, animated: true)
    }
    
}
