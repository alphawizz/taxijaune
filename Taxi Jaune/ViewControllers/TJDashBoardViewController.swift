//
//  TJDashBoardViewController.swift
//  Taxi Jaune
//
//  Created by shaifali bangar on 23/05/20.
//  Copyright © 2020 shaifali bangar. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class TJDashBoardViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {

     var locationManager = CLLocationManager()
     @IBOutlet weak var mapView: GMSMapView!
    var currentLatitude = String()
       var currentLongitude = String()
       
       var arrayMapMarkers : [GMSMarker] = []
       var marker = GMSMarker()
       
       var mapMarkers : [[String : Any]] = [["latitude":22.7196,
                                             "longitude" : 75.8577],
                                            ["latitude":23.2599,
                                             "longitude" : 77.4126]]
       
       var destinationLocation = GMSMarker()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        mapView.clear()
        determineMyCurrentLocation()
    }
    

    @IBAction func onMenu(_ sender: Any) {
          sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func onDestination(_ sender: Any) {
       showAlertPopupForBookingDetail()
        
    }
    
    
    // Mark -> CLLocation Delegate...
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation:CLLocation = locations[0] as CLLocation
        self.mapView.isMyLocationEnabled = true
        let camera = GMSCameraPosition.camera(withLatitude: userLocation.coordinate.latitude, longitude:userLocation.coordinate.longitude, zoom:15)
        currentLatitude = "\(userLocation.coordinate.latitude)"
        currentLongitude = "\( userLocation.coordinate.longitude)"
        self.mapView.camera = camera
        
         self.locationManager.stopUpdatingLocation()
        callGetMapLocationApi()
        
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        self.mapView?.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func callGetMapLocationApi()  {
        
        mapView.clear()
        
        for dict in  mapMarkers {
            
            let latitude = dict["latitude"] ?? 0.0
            let longitude = dict["longitude"] ?? 0.0
            
            let title = "hello"
            let address = "Find me!!!!"
            
            let position = CLLocationCoordinate2D(latitude: latitude as! CLLocationDegrees , longitude: longitude as! CLLocationDegrees)
            let locationmarker = GMSMarker(position: position)
            locationmarker.map = self.mapView
            
            var imageView : UIImageView
            imageView  = UIImageView(frame:CGRect(x:0, y:0, width:30, height:35));
            imageView.image = UIImage.init(named: "currentLocation")
            
            locationmarker.iconView = imageView
            locationmarker.snippet = address
            locationmarker.title = title
            
            self.arrayMapMarkers.append(locationmarker)
        }
        
       // draw()
        
    }
    
    func showAlertPopupForBookingDetail()  {
        
        let popupVC = FSStoryBoard.Main.instantiateViewController(withIdentifier: String.init(describing: TJMapPopupViewController.self)) as! TJMapPopupViewController
       // popupVC.strMessage = strMessage
       // popupVC.delegate = (self as! TJEmailViewControllerDelegate)
        popupVC.modalPresentationStyle = .overFullScreen
        self.present(popupVC, animated: true, completion: nil)
    }

}

extension TJDashBoardViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        let  placeLatitude =  place.coordinate.latitude
        let  placeLongitude =  place.coordinate.longitude
        
        let destination = mapMarkers.last
        
        let locationSource = CLLocationCoordinate2D(latitude: placeLatitude , longitude: placeLongitude )
        let locationDestination = CLLocationCoordinate2D(latitude: destination!["latitude"] as! CLLocationDegrees, longitude: destination!["latitude"] as! CLLocationDegrees)
       // self.getRouteSteps(from: locationSource, to: locationDestination)
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    }
    
    func showPlacePicker()  {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)    }
    
}
