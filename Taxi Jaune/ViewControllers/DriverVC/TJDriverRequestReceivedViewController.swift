//
//  TJDriverRequestReceivedViewController.swift
//  Taxi Jaune
//
//  Created by shaifali bangar on 26/05/20.
//  Copyright © 2020 shaifali bangar. All rights reserved.
//

import UIKit

class TJDriverRequestReceivedViewController: UIViewController {

    @IBOutlet var tableviewRequestList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableviewRequestList.register(UINib.init(nibName: String(describing: TJRequestRecivedTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: TJRequestRecivedTableViewCell.self))
          
        tableviewRequestList.register(UINib.init(nibName: String(describing: TJEmptyTableviewCellTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: TJEmptyTableviewCellTableViewCell.self))
          
          tableviewRequestList.reloadData()
          tableviewRequestList.tableFooterView = UIView()
       
    }
    
    @IBAction func onBack(_ sender: Any) {
    }
    
}
