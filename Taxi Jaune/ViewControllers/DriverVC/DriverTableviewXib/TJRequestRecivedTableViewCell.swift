//
//  TJRequestRecivedTableViewCell.swift
//  Taxi Jaune
//
//  Created by shaifali bangar on 26/05/20.
//  Copyright © 2020 shaifali bangar. All rights reserved.
//

import UIKit

class TJRequestRecivedTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension TJDriverRequestReceivedViewController : UITableViewDataSource, UITableViewDelegate {

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return 1
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let myRequestCell = tableviewRequestList.dequeueReusableCell(withIdentifier: String(describing: TJRequestRecivedTableViewCell.self)) as! TJRequestRecivedTableViewCell
    
   // let emptyCell = tableviewMyBooking.dequeueReusableCell(withIdentifier: String(describing: TJEmptyTableviewCellTableViewCell.self)) as! TJEmptyTableviewCellTableViewCell
    
    return myRequestCell
}

func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 135
}
}
