//
//  TJRentalVehiclesListViewController.swift
//  Taxi Jaune
//
//  Created by shaifali bangar on 26/05/20.
//  Copyright © 2020 shaifali bangar. All rights reserved.
//

import UIKit

class TJRentalVehiclesListViewController: UIViewController {

    @IBOutlet var tableviewVehicleList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

      tableviewVehicleList.register(UINib.init(nibName: String(describing: TJEmptyTableviewCellTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: TJEmptyTableviewCellTableViewCell.self))
        
        tableviewVehicleList.reloadData()
        tableviewVehicleList.tableFooterView = UIView()
    }
    

   @IBAction func onMenu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension TJRentalVehiclesListViewController : UITableViewDataSource, UITableViewDelegate {

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return 1
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let emptyCell = tableviewVehicleList.dequeueReusableCell(withIdentifier: String(describing: TJEmptyTableviewCellTableViewCell.self)) as! TJEmptyTableviewCellTableViewCell
    
    return emptyCell
}

func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
   return self.tableviewVehicleList.frame.size.height
    
}
}
