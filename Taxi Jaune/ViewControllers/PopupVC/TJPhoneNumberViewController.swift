//
//  TJPhoneNumberViewController.swift
//  Taxi Jaune
//
//  Created by shaifali bangar on 25/05/20.
//  Copyright © 2020 shaifali bangar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol TJPhoneNumberViewControllerDelegate  {
    func onPhoneNumberOK()
}



class TJPhoneNumberViewController: UIViewController {

    @IBOutlet var textfieldPhoneNumber: SkyFloatingLabelTextField!
    var delegate : TJPhoneNumberViewControllerDelegate!
    var strMessage = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onOK(_ sender: Any) {
              
              self.dismiss(animated: true) {
                    
                    if let delegate = self.delegate {
                              delegate.onPhoneNumberOK()
                          }
                }
          }
          
          @IBAction func onCancel(_ sender: Any) {
               self.dismiss(animated: true)
          }

    
}
