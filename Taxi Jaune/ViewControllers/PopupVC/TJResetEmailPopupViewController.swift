//
//  TJResetEmailPopupViewController.swift
//  Taxi Jaune
//
//  Created by shaifali bangar on 23/05/20.
//  Copyright © 2020 shaifali bangar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol TJResetEmailPopupViewControllerDelegate  {
    func onOK()
}

class TJResetEmailPopupViewController:  UIViewController {

 var delegate : TJResetEmailPopupViewControllerDelegate!
 var strMessage = String()
    
    @IBOutlet var textfieldPassword: SkyFloatingLabelTextField!
    
    @IBOutlet var textfieldNewPassword: SkyFloatingLabelTextField!
    
    @IBOutlet var textfieldConfirmPassword: SkyFloatingLabelTextField!
    
    
override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.
    
}

@IBAction func onOK(_ sender: Any) {
  
    self.dismiss(animated: true) {
        
        if let delegate = self.delegate {
                  delegate.onOK()
              }
    }
}
    
    @IBAction func onCancel(_ sender: Any) {
         self.dismiss(animated: true)
    }

}
