//
//  TJEmailViewController.swift
//  Taxi Jaune
//
//  Created by shaifali bangar on 25/05/20.
//  Copyright © 2020 shaifali bangar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol TJEmailViewControllerDelegate  {
    func onEmailOK()
}


class TJEmailViewController: UIViewController {

    @IBOutlet var textfieldEmail: SkyFloatingLabelTextField!
    
    var delegate : TJEmailViewControllerDelegate!
    var strMessage = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onOK(_ sender: Any) {
           
           self.dismiss(animated: true) {
                 
                 if let delegate = self.delegate {
                           delegate.onEmailOK()
                       }
             }
       }
       
       @IBAction func onCancel(_ sender: Any) {
            self.dismiss(animated: true)
       }

}
