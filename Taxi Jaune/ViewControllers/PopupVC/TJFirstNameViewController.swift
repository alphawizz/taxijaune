//
//  TJFirstNameViewController.swift
//  Taxi Jaune
//
//  Created by shaifali bangar on 25/05/20.
//  Copyright © 2020 shaifali bangar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol TJFirstNameViewControllerDelegate  {
    func onFirstNameOK()
}


class TJFirstNameViewController: UIViewController {

    @IBOutlet var textfieldFirstName: SkyFloatingLabelTextField!
    var delegate : TJFirstNameViewControllerDelegate!
       var strMessage = String()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
   @IBAction func onOK(_ sender: Any) {
        
        self.dismiss(animated: true) {
              
              if let delegate = self.delegate {
                        delegate.onFirstNameOK()
                    }
          }
    }
    
    @IBAction func onCancel(_ sender: Any) {
         self.dismiss(animated: true)
    }

}
