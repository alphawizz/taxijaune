//
//  TJHelpViewController.swift
//  Taxi Jaune
//
//  Created by shaifali bangar on 26/05/20.
//  Copyright © 2020 shaifali bangar. All rights reserved.
//

import UIKit

class TJHelpViewController: UIViewController {

    @IBOutlet var tableviewHelp: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

            tableviewHelp.register(UINib.init(nibName: String(describing: TJEmptyTableviewCellTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: TJEmptyTableviewCellTableViewCell.self))
              
              tableviewHelp.reloadData()
              tableviewHelp.tableFooterView = UIView()
    }

    @IBAction func onMenu(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}


extension TJHelpViewController : UITableViewDataSource, UITableViewDelegate {

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return 1
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let emptyCell = tableviewHelp.dequeueReusableCell(withIdentifier: String(describing: TJEmptyTableviewCellTableViewCell.self)) as! TJEmptyTableviewCellTableViewCell
    
    return emptyCell
}

func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return self.tableviewHelp.frame.size.height
    
}
}
