//
//  TJLoginViewController.swift
//  Taxi Jaune
//
//  Created by shaifali bangar on 22/05/20.
//  Copyright © 2020 shaifali bangar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import KRProgressHUD
import Toast_Swift

class TJLoginViewController: UIViewController {

    @IBOutlet var textfieldPhoneNumber: SkyFloatingLabelTextField!
    
    @IBOutlet var textfieldPassword: SkyFloatingLabelTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onLogin(_ sender: Any) {
        
        navigateToDashBoardScreen()
    }
    
    @IBAction func onSignup(_ sender: Any) {
        self.performSegue(withIdentifier: "TJSignupViewController", sender: nil)
    }
    
    func navigateToDashBoardScreen()  {
           
           let main = UIStoryboard(name: "Main", bundle: nil)
           
           let objHome = main.instantiateViewController(withIdentifier: "TJDashBoardViewController") as! TJDashBoardViewController
           
           let navigationController = NavigationController(rootViewController: objHome)
           let mainViewController = MainViewController()
           
           mainViewController.rootViewController = navigationController
           navigationController.navigationBar.isHidden = true
           
           mainViewController.setup(type: UInt(1))
           
           if #available(iOS 10.0, *) {
               mainViewController.leftViewBackgroundBlurEffect = UIBlurEffect(style: .regular)
           } else {
               // Fallback on earlier versions
           }
           self.navigationController?.pushViewController(mainViewController, animated: false)
       }
    

}
