//
//  TJMyBookingViewController.swift
//  Taxi Jaune
//
//  Created by shaifali bangar on 26/05/20.
//  Copyright © 2020 shaifali bangar. All rights reserved.
//

import UIKit

class TJMyBookingViewController: UIViewController {

    @IBOutlet var tableviewMyBooking: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableviewMyBooking.register(UINib.init(nibName: String(describing: TJMyBookingTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: TJMyBookingTableViewCell.self))
        
        tableviewMyBooking.register(UINib.init(nibName: String(describing: TJEmptyTableviewCellTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: TJEmptyTableviewCellTableViewCell.self))
        
        tableviewMyBooking.reloadData()
        tableviewMyBooking.tableFooterView = UIView()
    }
    
     @IBAction func onMenu(_ sender: Any) {
          self.navigationController?.popViewController(animated: true)
      }

}
