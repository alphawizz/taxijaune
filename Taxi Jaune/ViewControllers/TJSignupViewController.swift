//
//  TJSignupViewController.swift
//  Taxi Jaune
//
//  Created by shaifali bangar on 22/05/20.
//  Copyright © 2020 shaifali bangar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import KRProgressHUD
import Toast_Swift

class TJSignupViewController: UIViewController {

    @IBOutlet var labelFirstName: SkyFloatingLabelTextField!
    
    @IBOutlet var labelPhoneNumber: SkyFloatingLabelTextField!
    
    @IBOutlet var textfieldPassword: SkyFloatingLabelTextField!
    
    @IBOutlet var textfieldConfirmPassword: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onSignup(_ sender: Any) {
    }
    
    @IBAction func onLogin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
