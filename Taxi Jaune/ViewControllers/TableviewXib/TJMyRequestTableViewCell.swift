//
//  TJMyRequestTableViewCell.swift
//  Taxi Jaune
//
//  Created by shaifali bangar on 23/05/20.
//  Copyright © 2020 shaifali bangar. All rights reserved.
//

import UIKit

class TJMyRequestTableViewCell: UITableViewCell {

    @IBOutlet var labelDistance: UILabel!
    @IBOutlet var labelNumber: UILabel!
    @IBOutlet var labelDate: UILabel!
    @IBOutlet var labelSource: UILabel!
    @IBOutlet var labelDestination: UILabel!
    @IBOutlet var onDetail: DesignableButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension TJMyRequestViewController : UITableViewDataSource, UITableViewDelegate {

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return 1
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let myRequestCell = tableviewMyRequest.dequeueReusableCell(withIdentifier: String(describing: TJMyRequestTableViewCell.self)) as! TJMyRequestTableViewCell
    
    myRequestCell.onDetail.tag = indexPath.row
    
    myRequestCell.onDetail.addTarget(self, action: #selector(self.onDetail), for: .touchUpInside)
    
//    let emptyCell = tableviewMyRequest.dequeueReusableCell(withIdentifier: String(describing: TJEmptyTableviewCellTableViewCell.self)) as! TJEmptyTableviewCellTableViewCell
    
    return myRequestCell
}

func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 180
    
}
    
    @objc func onDetail(buttonDelete : UIButton)  {
        
            let popupVC = FSStoryBoard.Main.instantiateViewController(withIdentifier: String.init(describing: TJMyRequestDetailPopupViewController.self)) as! TJMyRequestDetailPopupViewController
           // popupVC.strMessage = strMessage
           // popupVC.delegate = (self as! TJEmailViewControllerDelegate)
            popupVC.modalPresentationStyle = .overFullScreen
            self.present(popupVC, animated: true, completion: nil)
    }
}
