//
//  TJMyRequestViewController.swift
//  Taxi Jaune
//
//  Created by shaifali bangar on 23/05/20.
//  Copyright © 2020 shaifali bangar. All rights reserved.
//

import UIKit

class TJMyRequestViewController: UIViewController {

    @IBOutlet var tableviewMyRequest: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

       tableviewMyRequest.register(UINib.init(nibName: String(describing: TJMyRequestTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: TJMyRequestTableViewCell.self))
        
      tableviewMyRequest.register(UINib.init(nibName: String(describing: TJEmptyTableviewCellTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: TJEmptyTableviewCellTableViewCell.self))
        
        tableviewMyRequest.reloadData()
        tableviewMyRequest.tableFooterView = UIView()
    }
    
    @IBAction func onMenu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
