//
//  TJMyProfileViewController.swift
//  Taxi Jaune
//
//  Created by shaifali bangar on 23/05/20.
//  Copyright © 2020 shaifali bangar. All rights reserved.
//

import UIKit

class TJMyProfileViewController: UIViewController,
TJResetEmailPopupViewControllerDelegate, TJFirstNameViewControllerDelegate,
TJPhoneNumberViewControllerDelegate, TJLastNameViewControllerDelegate,TJEmailViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onMenu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onEditLastName(_ sender: Any) {
        showAlertPopupForEditLastName(strMessage: "")
    }
    
    @IBAction func onEditFirstName(_ sender: Any){
         showAlertPopupForEditFirstName(strMessage: "")
    }
    
    @IBAction func onEditMobile(_ sender: Any) {
         showAlertPopupForEditPhoneNumber(strMessage: "")
    }
    
    @IBAction func onEditEmail(_ sender: Any) {
         showAlertPopupForEditEmail(strMessage: "")
    }
    
    @IBAction func onEditPassword(_ sender: Any) {
        
        showAlertPopupForEditPassword(strMessage: "")
    }
    
    //Mark -> Delegate method of edit Password
    func onOK() {
        
    }
    
    func onFirstNameOK() {
        
    }
    
    func onPhoneNumberOK() {
        
    }
    
    func onLastNameOK() {
        
    }
    
    func onEmailOK() {
        
    }
    
}

extension TJMyProfileViewController {
    
    func showAlertPopupForEditPassword(strMessage : String)  {
           
           let popupVC = FSStoryBoard.Main.instantiateViewController(withIdentifier: String.init(describing: TJResetEmailPopupViewController.self)) as! TJResetEmailPopupViewController
           popupVC.strMessage = strMessage
        popupVC.delegate = self as TJResetEmailPopupViewControllerDelegate
           popupVC.modalPresentationStyle = .overFullScreen
           self.present(popupVC, animated: true, completion: nil)
       }
    
    func showAlertPopupForEditFirstName(strMessage : String)  {
        
        let popupVC = FSStoryBoard.Main.instantiateViewController(withIdentifier: String.init(describing: TJFirstNameViewController.self)) as! TJFirstNameViewController
        popupVC.strMessage = strMessage
        popupVC.delegate = (self as! TJFirstNameViewControllerDelegate)
        popupVC.modalPresentationStyle = .overFullScreen
        self.present(popupVC, animated: true, completion: nil)
    }
    
    func showAlertPopupForEditLastName(strMessage : String)  {
        
        let popupVC = FSStoryBoard.Main.instantiateViewController(withIdentifier: String.init(describing: TJLastNameViewController.self)) as! TJLastNameViewController
        popupVC.strMessage = strMessage
        popupVC.delegate = (self as! TJLastNameViewControllerDelegate)
        popupVC.modalPresentationStyle = .overFullScreen
        self.present(popupVC, animated: true, completion: nil)
    }
    
    func showAlertPopupForEditPhoneNumber(strMessage : String)  {
        
        let popupVC = FSStoryBoard.Main.instantiateViewController(withIdentifier: String.init(describing: TJPhoneNumberViewController.self)) as! TJPhoneNumberViewController
        popupVC.strMessage = strMessage
        popupVC.delegate = (self as! TJPhoneNumberViewControllerDelegate)
        popupVC.modalPresentationStyle = .overFullScreen
        self.present(popupVC, animated: true, completion: nil)
    }
    
    func showAlertPopupForEditEmail(strMessage : String)  {
        
        let popupVC = FSStoryBoard.Main.instantiateViewController(withIdentifier: String.init(describing: TJEmailViewController.self)) as! TJEmailViewController
        popupVC.strMessage = strMessage
        popupVC.delegate = (self as! TJEmailViewControllerDelegate)
        popupVC.modalPresentationStyle = .overFullScreen
        self.present(popupVC, animated: true, completion: nil)
    }
}
